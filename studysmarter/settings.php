<?php
	defined('MOODLE_INTERNAL') || die;
	if ( $hassiteconfig ){
		global $DB;
		$settings = new admin_settingpage( 'local_studysmarter', get_string('settingstitle', 'local_studysmarter'));
		$ADMIN->add( 'localplugins', $settings );
		$settings->add(
		new admin_setting_configcheckbox('local_studysmarter_enable', get_string('studysmarterenablelabel', 'local_studysmarter'), get_string('studysmarterenablehelp', 'local_studysmarter'), 1)
		);
		
		$settings->add(
		new admin_setting_configtext('local_studysmarter_url', get_string('studysmarterurllabel', 'local_studysmarter'), get_string('studysmarterurlhelp', 'local_studysmarter'), 'https://prod.studysmarter.de/', PARAM_TEXT)
		);
	}	