<?php
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2019012941;              // The current plugin version (Date: YYYYMMDDXX).
$plugin->requires  = 2016051700;              // Requires this Moodle version. (3.5)
$plugin->component = 'local_studysmarter'; // Full name of the plugin (used for diagnostics).
$plugin->release   = '1.3 (2018072500)';
$plugin->maturity  = MATURITY_STABLE;
